const random_line = require("@melomario/random-line");

const startGame = () => {
  return {
    status: "RUNNING",
    word: "TESTE",
    lives: 5,
    display_word: "_ _ _ _ _",
    guesses: []
  };
};

const takeGuess = (game_state, guess) => {
  return {
    ...game_state,
    lives: game_state.lives - 1
  };
};

module.exports = {
  startGame,
  takeGuess
};
