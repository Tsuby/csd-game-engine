const game_engine = require("./lib/game_engine");

module.exports = {
  startGame: game_engine.startGame,
  takeGuess: game_engine.takeGuess
};
